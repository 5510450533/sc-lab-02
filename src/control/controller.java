package control;

import model.BankAccount;

public class controller {
	private BankAccount bankaccount;

	public controller(BankAccount bankaccount){
		this.bankaccount = bankaccount;
	}

	public double getBalance(){
		return bankaccount.getBalance();
	}

	public void withdraw(double amount)
	{   
		bankaccount.withdraw(amount);
	}

	public void deposit(double amount)
	{  
		bankaccount.deposit(amount);
	}
}
